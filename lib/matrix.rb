require 'faraday'
require 'faraday_middleware'

module Matrix
  class Client
    attr_reader :access_token

    BASE_URI = "/_matrix/client/r0"

    def initialize(server)
      @conn = Faraday.new(url: server) do |faraday|
        faraday.request  :json
        faraday.response :json
        faraday.adapter Faraday.default_adapter
      end

      @server = server if @conn
      @tx_id = 0

      @conn
    end

    def login(user, password)
      response = @conn.post "#{BASE_URI}/login",
      {
        "type": "m.login.password",
        "identifier": {
          "type": "m.id.user",
          "user": "#{user}"
        },
        "password": "#{password}"
      }

      @access_token = response.body["access_token"]
      @user         = user
      @since        = sync.body["next_batch"]

      response
    end

    def join(room_id)
      response = @conn.post "#{BASE_URI}/rooms/#{room_id}/join",
        {},
        { "Authorization": "Bearer #{@access_token}" }

      @room_id = response.body["room_id"] if response.status == 200
      response
    end

    def create_pm(user_id)
      response = @conn.post "#{BASE_URI}/createRoom",
        {
          "preset": "trusted_private_chat",
          "invite": [user_id],
          "creation_content": {
            "m.federate": false
          }
        },
        { "Authorization": "Bearer #{@access_token}" }

      response
    end

    def invite(room_id, user_id)
      response = @conn.post "#{BASE_URI}/rooms/#{room_id}/invite",
        {
          "user_id": user_id
        },
        { "Authorization": "Bearer #{@access_token}" }

      response
    end

    def send(message)
      response = @conn.put "#{BASE_URI}/rooms/#{@room_id}/send/m.room.message/#{tx_id}",
        {
          "body": "#{message}",
          "msgtype": "m.text"
        },
        { "Authorization": "Bearer #{@access_token}" }

      response
    end

    def sync(since=@since)
      response = @conn.get "#{BASE_URI}/sync",
        {
          "since": since
        },
        { "Authorization": "Bearer #{@access_token}" }

      @since = response.body["next_batch"]

      response
    end

    def messages_from(room_id: @room_id, since: @since, sender_id: whoami)
      updates = sync
      return false if updates.body["rooms"]["join"].empty?

      response = updates.body["rooms"]["join"]["#{room_id}"]["timeline"]["events"]
        .select { |e| e["type"] == "m.room.message" }
        .select { |m| m["sender"] == sender_id }

      response.empty? ? false : response
    end

    def whoami
      response = @conn.get do |req|
        req.url  "#{BASE_URI}/account/whoami"
        req.body = { "user_id": "@#{@user}:#{@server}" }
        req.headers["Authorization"] = "Bearer #{@access_token}"
      end
    end

    def tx_id
      r = @tx_id.to_s + (Time.now.to_i * 1000).to_s
      @tx_id += 1
      r
    end

    def get_display_name(user_id)
      response = @conn.get "#{BASE_URI}/profile/#{user_id}/displayname",
        {
        },
        { "Authorization": "Bearer #{@access_token}" }

      response
    end
  end
end
