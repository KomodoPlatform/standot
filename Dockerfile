FROM ruby:2.3.7-slim

WORKDIR /standot

COPY . /standot

RUN apt-get update && apt-get install -qq -y build-essential libsqlite3-dev --fix-missing --no-install-recommends

RUN bundle

EXPOSE 4567

ENV DOCKER_LOGS="true"

CMD ["ruby", "app.rb"]
