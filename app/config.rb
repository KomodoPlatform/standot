module Config
  extend self

  def parameter(*names)
    names.each do |name|
      attr_accessor name
      define_method name do |*values|
        value = values.first
        value ? self.send("#{name}=", value) : instance_variable_get("@#{name}")
      end
    end
  end

  def settings(&block)
    instance_eval &block
  end

  def load(file)
    YAML.load(File.read(file)).each do |k, v|
      Config.send(:"#{k}=", v)
    end
  end
end

Config.settings do
  parameter :server_uri
  parameter :bot_username, :bot_password
  parameter :questions, :pause, :timeout, :done, :not_done
  parameter :report_room
  parameter :start_time, :quit_time
  parameter :log_file
  parameter :port, :bind
  parameter :ui_username, :ui_password
end
