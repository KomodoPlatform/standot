require 'data_mapper'
require 'dm-sqlite-adapter'

require 'rufus-scheduler'

require 'erb'

require 'logger'

if ENV['DOCKER_LOGS']
  fd = IO.sysopen("/proc/1/fd/1", "w")
  io = IO.new(fd, "w")
  io.sync = true
  LOG_FILE = io
else
  LOG_FILE = STDOUT
end

$LOG = Logger.new(LOG_FILE)
$LOG.level = Logger::DEBUG

unless File.exists? "#{Dir.pwd}/db"
  $LOG.info "Warning: Database path \"db/\" does not exist. Creating."
  Dir.mkdir "#{Dir.pwd}/db"
end

require_relative '../lib/matrix'
require_relative '../app/model'
require_relative '../app/collector'
require_relative '../app/config'

# Initialize default settings
Config.settings do
  server_uri   "https://matrix.org"
  bot_username ENV["BOT_USERNAME"]
  bot_password ENV["BOT_PASSWORD"]
  report_room  ENV["REPORT_ROOM"]
  questions    [
    "What are you planning to do today?",
    "Why is it important?",
    "Are there any impediments that team can help with?",
  ]
  pause        3
  timeout      9
  start_time   "00 10 * * *"
  quit_time    "23:59"
  done         "Your standup has been recorded. Happy hacking!"
  not_done     "You have not participated in daily standup."
  log_file     ""
  port         4567
  bind         "0.0.0.0"
  ui_username  "participant"
  ui_password  ""
end

custom_config = File.expand_path("../../config.yml", __FILE__)

Config.load(custom_config) if File.exists?(custom_config)
