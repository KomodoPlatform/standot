DataMapper::setup(:default, "sqlite3://#{Dir.pwd}/db/database.db")

class User
  include DataMapper::Resource

  property :id,             Serial, lazy: false
  property :user_id,        String, lazy: false
  property :display_name,   String, lazy: false
  property :room_id,        String, lazy: false
  property :created_at,     DateTime, lazy: false

  validates_uniqueness_of   :user_id, :room_id

  has n, :standups

  def slug
    /\A@(.*):.*\Z/
      .match(user_id)[1]
      .gsub(/\W/,'_')
  end

  def self.by_slug(slug)
    all(
      :user_id.like => "@#{slug}:%"
    ).first
  end

  def last_standup
    standups.all(
      limit: Config.questions.count,
      order: [ :created_at.desc ]
    )
      .map { |s| s.to_s }.reverse
  end

  def last_standup_empty?
    last_standup.reject { |answer| answer[:answer] == "" }.empty?
  end

  def by_date(date)
    standups.by_date(date)
  end

  def years
    standups
      .all(order: [:created_at.desc])
      .map { |s| s.created_at.strftime("%Y") }
      .uniq
      .reverse
  end

  def months(year = DateTime.now.year)
    year = year.to_i

    standups
      .all(order: [:created_at.desc])
      .select { |s| s.created_at.year == year }
      .map { |s| [ "#{s.created_at.strftime("%b")}", s.created_at.strftime("%m") ] }
      .uniq
      .reverse
  end


  def dates(
    year  = DateTime.now.year,
    month = DateTime.now.month
  )
    year  = year.to_i
    month = month.to_i

    standups
      .all(order: [:created_at.desc])
      .select { |s| s.created_at.year == year }
      .select { |s| s.created_at.month == month }
      .map { |s| s.created_at.strftime("%Y/%m/%d") }
      .uniq
  end

  def self.all_standups_by_date(date)
    User.all.map do |user|
      {
        display_name: "#{user.display_name}",
        answers: user.by_date(date)
      }
    end
  end

  def verified?
    display_name != nil && room_id != nil
  end

  def last_seen
    last_standup = standups
      .all(order: [:created_at.desc])
      .first

    last_standup.nil? ? created_at : last_standup.created_at
  end

  def self.verified_all
    all(
      :display_name.not => "",
      :room_id.not => ""
    )
  end

  def self.participants
    User.all.sort { |u, v| u.last_seen <=> v.last_seen }.reverse
  end
end

class Standup
  include DataMapper::Resource

  property :id,         Serial, lazy: false
  property :question,   Text, lazy: false
  property :answer,     Text, lazy: false
  property :created_at, DateTime, lazy: false

  belongs_to :user

  def self.by_date(date)
    day_starts_at = DateTime.new(date.year, date.month, date.day, 0, 0, 0)
    day_ends_at   = DateTime.new(date.year, date.month, date.day, 23, 59, 59)

    all(
      :created_at.gte => day_starts_at,
      :created_at.lte => day_ends_at,
      order: [ :created_at.desc ]
    )
      .map { |s| s.to_s }.reverse
  end

  def to_s
    {
      question:   question,
      answer:     answer,
      created_at: created_at
    }
  end

  def self.years
    all(order: [:created_at.desc])
      .map { |s| s.created_at.strftime("%Y") }
      .uniq
      .reverse
  end

  def self.months(year = DateTime.now.year)
    year = year.to_i

    all(order: [:created_at.desc])
      .select { |s| s.created_at.year == year }
      .map { |s| [ "#{s.created_at.strftime("%b")}", s.created_at.strftime("%m") ] }
      .uniq
      .reverse
  end

  def self.dates(
    year  = DateTime.now.year,
    month = DateTime.now.month
  )
    year  = year.to_i
    month = month.to_i

    all(order: [:created_at.desc])
      .select { |s| s.created_at.year == year }
      .select { |s| s.created_at.month == month }
      .map { |s| s.created_at.strftime("%Y/%m/%d") }
      .uniq
  end
end

DataMapper.finalize.auto_upgrade!
