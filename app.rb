require 'sinatra'

require_relative 'app/application'

$LOG.info "Application has started with bot \"#{Config.bot_username}\""
$LOG.info "Standups will be collected on cron schedule: \"#{Config.start_time}\""

scheduler = Rufus::Scheduler.singleton

scheduler.cron "#{Config.start_time}" do

  $LOG.info "It's time for daily standups. Collecting answers..."

  User.all.each do |user|
    Thread.new {
      collector = StandupCollector.new(user)
      collector.setup && collector.get_standup_answers
    }
  end
end

set :port, Config.port
set :bind, Config.bind

use Rack::Auth::Basic, "Authorize to Standot" do |username, password|
  username == Config.ui_username && password == Config.ui_password
end unless Config.ui_password.empty?

get '/' do
  redirect to ("/today")
end

get '/today' do
  date = DateTime.now

  day = {
    date: date,
    by_users: User.all_standups_by_date(date)
  }

  erb :day, locals: { day: day }, layout: :layout
end

get '/standups' do
  redirect to("/standups/#{DateTime.now.year}/#{DateTime.now.month}")
end

get '/standups/:year/:month' do
  return erb :_404 unless params_valid?(params)

  year  = params["year"].to_i
  month = params["month"].to_i

  months = Standup.months(year)
  years  = Standup.years

  dates = Standup.dates(year, month).map do |d|
    y, m, dy = d.split('/').map { |s| s.to_i }
    DateTime.new(y, m, dy)
  end

  month = DateTime.new(0,month,1).strftime("%B")

  erb :standups,
      locals: { dates: dates, year: year, month: month, years: years, months: months },
      layout: :layout
end

get '/settings' do
  users = User.all.map { |u| u.slug }
  erb :register,
    locals: { users: users },
    layout: :layout
end

get '/settings/remove/:user' do
  return erb :_404 unless params_valid?(params)

  erb :remove,
    locals: { user: params["user"] },
    layout: :layout
end

post '/settings/remove/:user' do
  return erb :_404 unless params_valid?(params)

  user = User.by_slug params["user"]

  if ! user.nil? && user.destroy
    $LOG.info "User \"#{user.user_id}\" has been removed."

    erb :message,
      locals: {
        title: "User Removed",
        message: "User #{user.user_id} has been removed.",
        back_to: "/settings",
        back_to_msg: "Back to Settings"
      },
      layout: :layout
  else
    erb :message,
      locals: {
        title: "User Not Removed",
        message: "User #{params["user"]} could not be removed.",
        back_to: "/settings",
        back_to_msg: "Back to Settings"
      },
      layout: :layout
  end
end

post '/create' do
  return erb :_404 unless params_valid?(params)

  user_id = params["user_id"]

  if User.first(user_id: user_id)
    return erb :message,
      locals: {
      title: "Participant Exists",
      message: "The participant \"#{user_id}\" already exists.",
      back_to: "#{User.first(user_id: user_id).slug}",
      back_to_msg: "View Participant"
    },
    layout: :layout
  end

  user = User.create(user_id: user_id, display_name: "", room_id: "")

  collector = StandupCollector.new(user)
  collector.setup

  if user.display_name && user.room_id && collector.invite && user.save
    $LOG.info "Participant \"#{user.display_name}\" has been created."

    erb :message,
      locals: {
        title: "Participant Registered",
        message: "Participant \"#{user.display_name}\" has been created.",
        back_to: "/participants",
        back_to_msg: "Back to Participants"
      },
      layout: :layout
  else
    user.destroy

    $LOG.info "Participant \"#{params["user_id"]}\" cannot be created."

    erb :message,
      locals: {
        title: "Registration Failure",
        message: "Participant \"#{params["user_id"]}\" cannot be created. Check logs for more information.",
        back_to: "/settings",
        back_to_msg: "Back to Settings"
      },
      layout: :layout
  end
end

get '/participants' do
  users = User.participants

  erb :participants,
    locals: { users: users },
    layout: :layout
end

get '/:user' do
  return erb :_404 unless params_valid?(params)

  user = User.by_slug params["user"]
  if user
    redirect to("/#{user.slug}/#{user.last_seen.year}/#{user.last_seen.month}")
  end
end

get '/:user/:year/:month' do
  return erb :_404 unless params_valid?(params)

  year  = params["year"].to_i
  month = params["month"].to_i

  user = User.by_slug params["user"]

  if user
    months = user.months(year)
    years  = user.years

    days = user.dates(year, month).map do |d|
      y, m, dy = d.split('/').map { |s| s.to_i }
      DateTime.new(y, m, dy)
    end

    month = DateTime.new(0,month,1).strftime("%B")

    erb :user,
      locals: { days: days,
                year: year,
                month: month,
                years: years,
                months: months,
                user: user.display_name,
                slug: user.slug },
      layout: :layout
  else
    erb :message,
      locals: {
        title: "User Not Found",
        message: "User #{params["user"]} not found.",
        back_to: "/participants",
        back_to_msg: "Back to Participants"
      },
      layout: :layout
  end
end

get '/day/:year/:month/:date' do
  return erb :_404 unless params_valid?(params)

  date = DateTime.new(
    params["year"].to_i, params["month"].to_i, params["date"].to_i
  )

  day = {
    date: date,
    by_users: User.all_standups_by_date(date)
  }

  erb :day, locals: { day: day }, layout: :layout
end

get '/:user/:year/:month/:date' do
  return erb :_404 unless params_valid?(params)

  date = DateTime.new(
    params["year"].to_i, params["month"].to_i, params["date"].to_i
  )

  user = User.by_slug params["user"]

  answers = user.by_date(date)

  erb :standup,
    locals: { answers: answers, user: user.display_name, date: date},
    layout: :layout
end

def params_valid?(params)
  return true if params.empty?
  params.map do |k, v|
    case k
    when "date"
      v.to_i >= 1 && v.to_i <= 31 ? true : false
    when "month"
      v.to_i >= 1 && v.to_i <= 12 ? true : false
    when "year"
      /\A[0-9]{4}\Z/.match(v).nil? ? false : true
    when "user"
      /\w+/.match(v).nil? ? false : true
    when "user_id"
      /\A@.+:\w+\.\w+\Z/.match(v).nil? ? false : true
    else
      false
    end
  end.include?(false) ? false : true
end
